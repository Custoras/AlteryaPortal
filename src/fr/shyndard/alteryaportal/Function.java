package fr.shyndard.alteryaportal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaportal.method.Area;
import fr.shyndard.alteryaportal.method.Destination;

public class Function {

	public static void loadDestination() {
		Portal.getDestinationList().clear();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT * FROM portal_destination");
			while(result.next()) {
				try {
					World world = Bukkit.getWorld(result.getString("destination_world"));
					Float x = result.getFloat("destination_x");
					Float y = result.getFloat("destination_y");
					Float z = result.getFloat("destination_z");
					Integer yaw = result.getInt("destination_yaw");
					Integer pitch = result.getInt("destination_pitch");
					Location loc = new Location(world, x, y, z, yaw, pitch);
					Portal.getDestinationList().add(new Destination(result.getString("destination_name"), loc, result.getString("destination_item").split(":")));
				} catch (Exception ex) { ex.printStackTrace(); }
			}
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static Area getPortalArea() {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT * FROM portal");
			while(result.next()) {
				try {
					World world = Bukkit.getWorld(result.getString("portal_pos_world"));
					Float x1 = result.getFloat("portal_pos1_x");
					Float y1 = result.getFloat("portal_pos1_y");
					Float z1 = result.getFloat("portal_pos1_z");
					Float x2 = result.getFloat("portal_pos2_x");
					Float y2 = result.getFloat("portal_pos2_y");
					Float z2 = result.getFloat("portal_pos2_z");
					return new Area(new Location(world, x1, y1, z1), new Location(world, x2, y2, z2));
				} catch (Exception ex) { ex.printStackTrace(); }
			}
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}

	public static void setDestination(String name, String item, Location loc) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE portal_destination SET destination_item = '"+item+"', destination_world = '"+loc.getWorld().getName()+"', destination_x = '"+loc.getX()+"', destination_y = '"+loc.getY()+"', destination_z = '"+loc.getZ()+"', destination_yaw = '"+loc.getYaw()+"', destination_pitch = '"+loc.getPitch()+"' WHERE destination_name = '"+name+"'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}		
		loadDestination();
		Portal.actualizeMenu();
	}

	public static void createDestination(String name, String item, Location loc) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO portal_destination VALUES('"+name+"', '"+item+"', '"+loc.getWorld().getName()+"', '"+loc.getX()+"', '"+loc.getY()+"', '"+loc.getZ()+"', '"+loc.getYaw()+"', '"+loc.getPitch()+"')");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}	
		loadDestination();
		Portal.actualizeMenu();
	}

	public static void delDestination(String name) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM portal_destination WHERE portal_name = '"+name+"'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		loadDestination();
		Portal.actualizeMenu();
	}

	public static void setPortal(Location loc1, Location loc2) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE portal SET portal_pos_world = '"+loc1.getWorld().getName()+"', portal_pos1_x = '"+loc1.getX()+"', portal_pos1_y = '"+loc1.getY()+"', portal_pos1_z = '"+loc1.getZ()+"', portal_pos2_x = '"+loc2.getX()+"', portal_pos2_y = '"+loc2.getY()+"', portal_pos2_z = '"+loc2.getZ()+"'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		Portal.init();
	}
}
