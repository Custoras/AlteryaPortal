package fr.shyndard.alteryaportal;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaportal.method.Destination;
import net.md_5.bungee.api.ChatColor;

public class CmdPortal implements CommandExecutor {

	String prefix = ChatColor.GRAY + "[Portail] ";
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(prefix + "La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("portal.admin")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if(args.length == 0) {
			sendHelp(sender);
			return false;
		}
		if(args[0].equalsIgnoreCase("reload")) {
			Function.loadDestination();
			Portal.init();
			sender.sendMessage(prefix + ChatColor.GREEN + "Chargement effectu�.");
			return false;
		}
		if(args[0].equalsIgnoreCase("setportal")) {
			Map<String, Location> player_marker = pi.getSelection();
			if(player_marker == null || player_marker.get("p1") == null || player_marker.get("p2") == null) {
				sender.sendMessage(prefix + ChatColor.RED + "Veuillez d�finir les marqueurs.");
				return false;
			}
			Function.setPortal(player_marker.get("p1"), player_marker.get("p2"));
			pi.getSelection().clear();
			sender.sendMessage(prefix + ChatColor.GREEN + "Emplacement du portail ajust�.");
			return false;
		}
		if(args[0].equalsIgnoreCase("setdestination")) {
			ItemStack item = player.getInventory().getItemInMainHand();
			if(item == null || item.getType() == Material.AIR) {
				sender.sendMessage(prefix + ChatColor.RED + "Prenez un item en main.");
				return false;
			}
			if(args.length == 2) {
				for(Destination destination : Portal.getDestinationList()) {
					if(destination.getName().equalsIgnoreCase(args[1])) {
						destination.setPosition(player.getLocation());
						Function.setDestination(destination.getName(), item.getType().name() + ":" + item.getData().getData(), player.getLocation());
						sender.sendMessage(prefix + ChatColor.GREEN + "Position modifi�e pour " + ChatColor.ITALIC + destination.getName() + ChatColor.GREEN + ".");
						return false;
					}
				}
				Function.createDestination(args[1], item.getType().name() + ":" + item.getData().getData(), player.getLocation());
				sender.sendMessage(prefix + ChatColor.GREEN + "Destination " + ChatColor.ITALIC + args[1] + ChatColor.GREEN + " cr��e.");
			} else {
				sender.sendMessage(prefix + ChatColor.GRAY + "Utilisation : /portal setdestination <nom_portail>");
			}
			return false;
		}
		if(args[0].equalsIgnoreCase("deldestination")) {
			if(args.length == 2) {
				for(Destination destination : Portal.getDestinationList()) {
					if(destination.getName().equalsIgnoreCase(args[1])) {
						Function.delDestination(args[1]);
						sender.sendMessage(prefix + ChatColor.GREEN + "Destination " + ChatColor.ITALIC + args[1] + ChatColor.GREEN + " supprim�e.");
						return false;
					}
				}
				sender.sendMessage(prefix + ChatColor.RED + "Aucune destination de ce nom trouv�e.");
			} else {
				sender.sendMessage(prefix + ChatColor.GRAY + "Utilisation : /portal deldestination <nom_portail>");
			}
			return false;
		}
		sendHelp(sender);
		return false;
	}
	
	void sendHelp(CommandSender sender) {
		sender.sendMessage(ChatColor.GRAY + "Commandes portail");
		sender.sendMessage(ChatColor.GRAY + "/portal reload");
		sender.sendMessage(ChatColor.GRAY + "/portal setportal");
		sender.sendMessage(ChatColor.GRAY + "/portal setdestination <nom_portail>");
		sender.sendMessage(ChatColor.GRAY + "/portal deldestination <nom_portail>");
	}

}
