package fr.shyndard.alteryaportal;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import fr.shyndard.alteryaportal.method.Area;
import fr.shyndard.alteryaportal.method.Destination;

public class Portal {

	private static Inventory inv;
	private static Area area;
	private static List<Destination> destination_list = new ArrayList<>();
	private static String name = ("" + ChatColor.DARK_PURPLE + ChatColor.BOLD + "TÚlÚporteur");
	
	public static void openMenu(Player player) {
		player.openInventory(inv);
	}
	
	public static List<Destination> getDestinationList() {
		return destination_list;
	}
	
	public static String getName() {
		return name;
	}
	
	public static void init() {
		if(inv == null) inv = Bukkit.createInventory(null, 9, name);
		actualizeMenu();
		area = Function.getPortalArea();
		area.open();
		area.playAnimation();
	}
	
	public static void actualizeMenu() {
		inv.clear();
		for(int i = 0; i < destination_list.size(); i++) {
			inv.addItem(destination_list.get((i)).getItem());
		}
	}

	public static void teleport(HumanEntity player, int destination_id) {
		Destination destination = destination_list.get(destination_id);
		player.teleport(destination.getLocation());
		player.sendMessage(ChatColor.GRAY + "TÚlÚportation vers " + ChatColor.GOLD + destination.getName() + ChatColor.GRAY + ".");
	}
	
	public static boolean isInside(Player player) {
		if(area == null) return false;
		return area.isInside(player.getLocation());
	}
}