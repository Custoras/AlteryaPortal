package fr.shyndard.alteryaportal.method;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Destination {

	private String name;
	private Location location;
	private ItemStack is;
	
	public Destination(String name, Location location, String[] item_arg) {
		this.name = name;
		this.location = location;
		try {
			if(item_arg.length == 1) {
				is = new ItemStack(Material.matchMaterial(item_arg[0]));
			} else if(item_arg.length == 2) {
				is = new ItemStack(Material.matchMaterial(item_arg[0]), 1, Byte.parseByte(item_arg[1]));
			}
		} catch(Exception ex) {
			is = new ItemStack(Material.MAP);
		}
		ItemMeta im = is.getItemMeta();
		im.setDisplayName("" +ChatColor.GRAY + ChatColor.ITALIC + "Direction " + ChatColor.GREEN + ChatColor.BOLD + name);
		is.setItemMeta(im);
	}
	
	public String getName() {
		return name;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public ItemStack getItem() {
		return is;
	}

	public void setPosition(Location location) {
		this.location = location;
	}
	
}
