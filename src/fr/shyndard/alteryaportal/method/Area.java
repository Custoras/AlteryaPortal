package fr.shyndard.alteryaportal.method;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import fr.shyndard.alteryaportal.Main;

public class Area {
	
	World world;
	int xmin;
	int xmax;
	int ymin;
	int ymax;
	int zmin;
	int zmax;
	
	public Area(Location loc1, Location loc2) {
		if(loc1.getBlockX() > loc2.getBlockX()) {
			xmin = loc2.getBlockX();
			xmax = loc1.getBlockX();
		} else {
			xmax = loc2.getBlockX();
			xmin = loc1.getBlockX();
		}
		if(loc1.getBlockY() > loc2.getBlockY()) {
			ymin = loc2.getBlockY();
			ymax = loc1.getBlockY();
		} else {
			ymax = loc2.getBlockY();
			ymin = loc1.getBlockY();
		}
		if(loc1.getBlockZ() > loc2.getBlockZ()) {
			zmin = loc2.getBlockZ();
			zmax = loc1.getBlockZ();
		} else {
			zmax = loc2.getBlockZ();
			zmin = loc1.getBlockZ();
		}
		world = loc1.getWorld();
	}
	
	public boolean isInside(Location loc) {
		return (loc.getBlockX() >= xmin && loc.getBlockX() <= xmax && loc.getBlockY() >= ymin && loc.getBlockY() <= ymax && loc.getBlockZ() >= zmin && loc.getBlockZ() <= zmax);
	}
	
	public Location getPos1() {
		return new Location(world, xmin, ymin, zmin);
	}
	
	public Location getPos2() {
		return new Location(world, xmax, ymax, zmax);
	}

	public void playAnimation() {
		new BukkitRunnable() {
			@Override
            public void run() {
				world.spawnParticle(Particle.VILLAGER_HAPPY, new Location(world, (xmax+xmin)/2, ymin, (zmax+zmin)/2+0.5), 20, 0.5, 2, 0.5);
				world.spawnParticle(Particle.PORTAL, new Location(world, (xmax+xmin)/2, ymin, (zmax+zmin)/2+0.5), 10, 1, 2, 1);
            }
		}.runTaskTimer(Main.getPlugin(), 0, 5L);
	}
	
	public void open() {
		setBlock(true);
	}

	public void close() {
		setBlock(false);
	}
	
	private void setBlock(boolean set) {
		for(int x = xmin; x<xmax; x++) {
			for(int z = zmin; z<zmax; z++) {
				for(int y = ymin; y<ymax; y++) {
					if(set) world.getBlockAt(x, y, z).setType(Material.PORTAL);
					else world.getBlockAt(x, y, z).setType(Material.AIR);
				}
			}
		}
	}
}
