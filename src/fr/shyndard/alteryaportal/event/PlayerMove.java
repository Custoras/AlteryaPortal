package fr.shyndard.alteryaportal.event;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaportal.Portal;

public class PlayerMove implements Listener {

	private Map<Player, Long> use_timer = new HashMap<>();
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		if(use_timer.get(event.getPlayer()) != null && use_timer.get(event.getPlayer()) + 5 >= System.currentTimeMillis()/1000) return;
		if(Portal.isInside(event.getPlayer())) {
			if(!DataAPI.getPlayer(event.getPlayer()).hasPermission("portal.access")) return;
			event.getPlayer().setVelocity(new Vector(0,0,0));
			Portal.openMenu(event.getPlayer());
			use_timer.put(event.getPlayer(), System.currentTimeMillis()/1000);
		}
	}
}
