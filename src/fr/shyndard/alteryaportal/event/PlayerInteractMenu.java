package fr.shyndard.alteryaportal.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.shyndard.alteryaportal.Portal;

public class PlayerInteractMenu implements Listener {

	@EventHandler
	public void onInteract(InventoryClickEvent event) {
		if(event.getClickedInventory() == null) return;
		if(event.getView().getTopInventory() != null 
				&& event.getView().getTopInventory().getName().startsWith(Portal.getName()) 
				&& !event.getClickedInventory().getName().startsWith(Portal.getName())
				&& event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)
		{
			event.setCancelled(true);
			return;
		}
		if(!event.getClickedInventory().getName().startsWith(Portal.getName())) return;
		event.setCancelled(true);
		if(Portal.getDestinationList().get(event.getSlot()) == null) return;
		event.getWhoClicked().closeInventory();
		Portal.teleport(event.getWhoClicked(), event.getSlot());
	}
}
