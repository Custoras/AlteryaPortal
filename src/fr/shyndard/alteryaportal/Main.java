package fr.shyndard.alteryaportal;

import org.bukkit.plugin.java.JavaPlugin;

import fr.shyndard.alteryaportal.event.PlayerInteractMenu;
import fr.shyndard.alteryaportal.event.PlayerMove;

public class Main extends JavaPlugin {

	static Main plugin;
	
	public void onEnable() {
		plugin = this;
		
		Function.loadDestination();
		Portal.init();
		
		getCommand("portal").setExecutor(new CmdPortal());
		
		getServer().getPluginManager().registerEvents(new PlayerMove(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractMenu(), this);
	}
	
	public static Main getPlugin() {
		return plugin;
	}
}
